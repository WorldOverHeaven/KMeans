// See https://aka.ms/new-console-template for more information

using System.Diagnostics;

namespace ConsoleAppKMeans;


public class Start
{
    public static void Main()
    {
        int dimension = 10;
        int clusterAmount = 95;
        int count = 10000;
        Stopwatch stopwatch = Stopwatch.StartNew();
        var pointSystem = new PointSystem(dimension, clusterAmount);
        pointSystem.GeneratePoints(count);
        var clusters = pointSystem.SplitOnClustersParallel();
        foreach (var c in clusters)
        {
            Console.WriteLine(c.Key);
            // foreach (var b in c.Value)
            // {
            //     Console.WriteLine(b);
            //     foreach (var a in b)
            //     {
            //         Console.WriteLine(a);
            //     }
            // }
        }
        Console.WriteLine(clusters["centers"]);
        Console.WriteLine("centers");
        foreach (var a in clusters["centers"])
        {
            // Console.Write(a[0]);
            // Console.Write("   ");
            // Console.WriteLine(a[1]);
            for (var j = 0; j < dimension; ++j)
            {
                Console.Write(a[j]);
                Console.Write("   ");
            }
            Console.WriteLine("");
        }
        stopwatch.Stop();
        Console.WriteLine(stopwatch.ElapsedMilliseconds);
    }
}


