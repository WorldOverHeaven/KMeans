namespace ConsoleAppKMeans;

public class Point
{
    private readonly int _id;
    private readonly List<Double> _coords;
    public int Dimension => _coords.Count;
    
    public int Id => _id;
    public double this[int i] => _coords[i];
    
    public Point(int id, List<double> coords)
    {
        _id = id;
        _coords = coords;
    }

    public void Print()
    {
        Console.WriteLine("ID");
        Console.WriteLine(_id);
        Console.WriteLine("Coords");
        foreach (var i in _coords)
        {
            Console.WriteLine(i);
        }
    }
    
    public static bool operator ==(Point a, Point b) => a.Id == b.Id;
    public static bool operator !=(Point a, Point b) => a.Id != b.Id;
    
    public static double Distance(Point p1, Point p2)
    {
        if (p1.Dimension != p2.Dimension)
        {
            Console.WriteLine("Error. Dimensions must be equal");
            return -1;
        }
        else
        {
            double result = 0;
            for (var i = 0; i < p1.Dimension; ++i)
            {
                result += (p1[i] - p2[i]) * (p1[i] - p2[i]);
            }

            result = Math.Sqrt(result);
            return result;
        }
    }
    
    public List<Double> GetAllComponents() {
        return _coords;
    }

}
